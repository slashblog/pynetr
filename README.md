# pynetr

This scripts enables one to see images uploaded using [netr-backend](https://gitlab.com/slashblog/netr-backend/).
To execute `python3 ipc-picture-viewer.py /location` where location is same as given in [netr-backend](https://gitlab.com/slashblog/netr-backend/).

## Installing dependencies

I don't use any other OS than GNU/Linux.
If your OS doesn't have these python packages install:

`pip install pillow dropbox tkinter`

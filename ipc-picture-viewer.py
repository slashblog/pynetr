#!/bin/python3

# Copyright 2022 author

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>


from datetime import datetime
import dropbox
import PIL.Image
import PIL.ImageTk
import sys
from tkinter import *

if len(sys.argv) == 1:
    print('Usage: %s <base-dropbox-path>' % sys.argv[0])
    sys.exit(0)

##########################################################################################
# Edit the following lines with right values #############################################
# START OF CONFIGURATION DATA ############################################################
##########################################################################################

# Dropbox access token :- generate a permanent one
DB_ACCESS_TOKEN = '__DB_ACCESS_TOKEN__'

##########################################################################################
# END OF CONFIGURATION DATA ##############################################################
##########################################################################################


LOCATION = sys.argv[1]
DB_ACCESS_TOKEN = 'oWIqe4LFb8EAAAAAAAAAASqcvrk2rMtAv7LkdiChNuhl1lf9UAf4gOd1L5er6H9P'

dbx = dropbox.Dropbox(DB_ACCESS_TOKEN)

def load_latest():
    global latest

    try:
        metadata, res = dbx.files_download(LOCATION + '/latest.json')
        latest = res.json()
    except dropbox.exceptions.ApiError as ex:
        print('Error accessing details of latest image metadata: ' + str(ex))
        sys.exit(1)
	
def exit():
    sys.exit(0)

image_cache = {}

def reload():
    global image_cache
    image_cache = {}
    load_latest()

def show_image(image):
    canvas.create_image(0, 0, image=image, anchor='nw')

def show_pic(cam, type, index):
    global image_cache
    try:
        location = '%s/%s-%s-%d-%s.jpg' % (latest['dest'], latest['time'], type, index, cam)
        if location in image_cache:
            print('Using image from cache')
            show_image(image_cache[location]['image2'])
            return

        print('Fetching: ' + location)
        metadata, res = dbx.files_download(location)
        if res.status_code == 200:
            fname = '/tmp/test.jpg'
            print('File name chosen as: ' + fname)
            with open(fname, 'wb') as f:
                f.write(res.content)
                image_cache[location] = {}
                image_cache[location]['image'] = PIL.Image.open(fname)
                image_cache[location]['resized'] = image_cache[location]['image'].resize((IMAGE_WIDTH, IMAGE_HEIGHT), PIL.Image.ANTIALIAS)
                image_cache[location]['image2'] = PIL.ImageTk.PhotoImage(image_cache[location]['resized'])
                canvas.create_image(0, 0, image=image_cache[location]['image2'], anchor='nw')
        else:
            print('Error: ' + str(res.status_code))
    except dropbox.exceptions.ApiError as ex:
        print('Error accessing image data for ' + cam + ': ' + str(ex))
        sys.exit(1)

load_latest()
ws = Tk()
WINDOW_WIDTH = ws.winfo_screenwidth()
WINDOW_HEIGHT = ws.winfo_screenheight()
WINDOW_LESS = 100
IMAGE_LESS = 120
IMAGE_WIDTH = WINDOW_WIDTH - IMAGE_LESS
IMAGE_HEIGHT = WINDOW_HEIGHT - IMAGE_LESS
print((WINDOW_WIDTH, WINDOW_HEIGHT))
print((IMAGE_WIDTH, IMAGE_HEIGHT))

ws.title('Cam Image Viewer')
ws.geometry(str(WINDOW_WIDTH - WINDOW_LESS) + 'x' + str(WINDOW_HEIGHT - WINDOW_LESS))
ws.config(bg='#cd950c')

button_frame = Frame(ws)

CAMS = latest['cameras']

for index, cam in enumerate(CAMS):        
    actionBtn = Button(
        button_frame,
        text=cam.capitalize(),
        padx=10,
        pady=10,
        command=lambda cam=cam, type='low', index=index + 1: show_pic(cam, type, index)
    )
    actionBtn.grid(row=0, column=index)

cancleBtn = Button(
    button_frame,
    text='Exit',
    padx=10,
    pady=10,
    command=exit    
)
cancleBtn.grid(row=0, column=len(CAMS))

reloadBtn = Button(
    button_frame,
    text='Reload',
    padx=10,
    pady=10,
    command=reload    
)
reloadBtn.grid(row=0, column=len(CAMS) + 1)

button_frame.pack(pady=10)

canvas = Canvas(
    ws,
    width=IMAGE_WIDTH,
    height=IMAGE_HEIGHT,
)

canvas.pack(fill=BOTH, expand=True)

ws.mainloop()


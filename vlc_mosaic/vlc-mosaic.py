#!/usr/bin/python3

# Copyright 2022 author

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>


import os
import platform
import sys
from PIL import Image

##########################################################################################
# Edit the following lines with right values #############################################
# START OF CONFIGURATION DATA ############################################################
##########################################################################################

# This assumes that you have forwarded all camera ports to a single machine.
# Add user, password, host, base port 
# Details about base port: If the camera ports are forwarded to 55541, 55542, 55543, 55544
# respectively. __BASE_PORT__ will have value 5554. While executing this script give port
# suffix values as command line arguments. Examples:
#   - ./vlc-mosaic 1 2 3 4
#   - ./vlc-mosaic 1 2 
#   - ./vlc-mosaic 1 2 3 4 5 6 7
# The script will automatically select the right mosaic based on number of port-suffix 
# specified on the command line.
URL = 'rtsp://__USER__:__PASSWORD__@__HOST__:__BASE_PORT__%s/Streaming/Channels/102/'

##########################################################################################
# END OF CONFIGURATION DATA ##############################################################
##########################################################################################


if len(sys.argv) < 3:
    print('Usage %s <port-suffix1> <port-suffix2> ...' % sys.argv[0])
    sys.exit(1)


MAX_X = int(os.environ['MAX_X'])
#MAX_Y = 1024
MAX_Y = int(os.environ['MAX_Y'])
MOSAIC = 'mosaic_vlc.vlm'

BG = 'bg.png'
VLC = '"c:\\Program Files\\VideoLAN\\VLC\\vlc.exe"'

if platform.system() != 'Windows':
    MOSAIC = '/tmp/' + MOSAIC
    VLC = 'vlc'
    BG = '/tmp/' + BG

count = len(sys.argv) - 1

m = 1
n = 1
while m * n < count:
    if m == n:
        m += 1
    else:
        n += 1

with open(MOSAIC, 'w') as file:
    file.write('# m => %d, n => %d\n' % (m, n))
    file.write('\n')
    file.write('del all\n\n')
    channels = []

    for i in range(1, count + 1):
        channel = 'ch%d' % i
        channels.append(channel)
        url = URL % sys.argv[i]
        file.write('new %s broadcast enabled\n' % channel)
        file.write('setup %s input "%s"\n' % (channel, url))
        file.write('setup %s output #mosaic-bridge{id=%s,width=%d,height=%d}\n' % (channel, channel, MAX_X / n, MAX_Y / m))
        file.write('\n')

    file.write('new bg broadcast enabled\n')
    file.write('setup bg input "%s"\n' % BG)
    file.write('setup bg option image-duration=-1\n')
    file.write(
        'setup bg output #transcode{vcodec=mp4v,vb=0,fps=0,acodec=none,channels=2,sfilter=mosaic{alpha=255,width=%d,height=%d,cols=%d,rows=%d,position=1,order="%s",keep-aspect-ratio=enabled,mosaic-align=0,keep-picture=1}}:bridge-in{offset=100}:display\n' % (MAX_X, MAX_Y,
        n, m, ','.join(channels)))
    file.write('\n')

    file.write('control bg play\n')
    for i in range(1, count + 1):
        file.write('control ch%d play\n' % i)
    file.write('\n')

    file.close()

if not os.path.exists(BG):
    img = Image.new('RGB', (MAX_X, MAX_Y))
    img.save(BG, 'PNG')

os.system('%s --no-dbus --vlm-conf %s' % (VLC, MOSAIC))

